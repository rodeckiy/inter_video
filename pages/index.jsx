import { useState } from 'react'
import Carousel from 'react-elastic-carousel'
export default function Home() {
  const allVideos = [],
    allCovers = [],
    [isPlayedVideo, setIsPlayedVideo] = useState(false),
    playVideo = key => {
      if (!isPlayedVideo) {
        setIsPlayedVideo(true)
        allCovers[key].hidden = true
        allVideos[key].play()
      }
    },
    playNextVideo = key => {
      allCovers[key - 1].hidden = false;
      if (videos.length > key) {
        allCovers[key].hidden = true;
        allVideos[key].play()
      }
    },
    videos = [
      {
        src: '/videos/1.mp4',
        cover: 'images/2.jpg'
      },
      {
        src: '/videos/1.mp4',
        cover: 'images/1.jpg'
      },
      {
        src: '/videos/1.mp4',
        cover: 'images/1.jpg'
      },
      {
        src: '/videos/1.mp4',
        cover: 'images/1.jpg'
      },
      {
        src: '/videos/1.mp4',
        cover: 'images/1.jpg'
      },
      {
        src: '/videos/1.mp4',
        cover: 'images/1.jpg'
      }
    ]
  return (
    <div className="container">
      <Carousel itemsToShow={3} initialFirstItem="1">
        {
          videos.map(({ src, cover }, key) =>
            <div className="video" key={key}>
              <img src={cover} ref={ref => allCovers[key] = ref} onMouseOver={() => playVideo(key)} />
              <video src={src} controls ref={ref => allVideos[key] = ref} muted onEnded={() => playNextVideo(key + 1)} autoPlay={false} />
            </div>
          )
        }
      </Carousel>
    </div>
  )
}
